package guiBankAcoount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TextField extends JFrame{
	private BankAccount bank;
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 200;
	private JButton withdrawButton ,depositButton;
	private ActionListener listener;
	private JLabel resultLabel;
	private JTextField amountField,outputField;
	private JPanel panel;
	
	public static void main(String[] args){
		new TextField();
	}
	public TextField(){
		
		bank = new BankAccount("Pond");
//		bank.deposit(1000);
//		bank.withdraw(100);
		
		createTextField();
		createButton();
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	private void createPanel() {
		resultLabel = new JLabel();
		
		panel = new JPanel();
		panel.add(resultLabel);
		panel.add(amountField);
		panel.add(withdrawButton);
		panel.add(depositButton);
		panel.add(resultLabel);
		add(panel);


		// TODO Auto-generated method stub
		
	}
	private void createButton() {
		withdrawButton = new JButton("withdraw");
		withdrawButton.addActionListener(listener);
		class withdrawListener implements ActionListener{
			public void actionPerformed
			(ActionEvent event){
				double amount = Double.parseDouble(amountField.getText());
				bank.withdraw(amount);
				resultLabel.setText("balance: " + bank.getBalance());
			}
		}

		depositButton = new JButton("deposit");
		depositButton.addActionListener(listener);
		class depositListener implements ActionListener{
			public void actionPerformed(ActionEvent event){
				double amount = Double.parseDouble(amountField.getText());
				bank.deposit(amount);
				resultLabel.setText("balance: " + bank.getBalance());
			}
		}

		// TODO Auto-generated method stub
		
	}
	private void createTextField() {
		final int FIELD_WIDTH = 10;
		amountField = new JTextField(FIELD_WIDTH);
		amountField.setText("   ");
		
		

	

	}


}
