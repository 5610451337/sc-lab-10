package guiBankAcoount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TextArea extends JFrame {
	private BankAccount bank;
	private static final int FRAME_WIDTH = 400;
	private static final int FRAME_HEIGHT = 250;
	private JButton withdrawButton ,depositButton;
	private ActionListener listener;
	private JTextArea resultArea;
	private JTextField amountField;
	private JPanel panel;
	private JScrollPane scroll;
	private static final int AREA_ROWS = 10;
	private static final int AREA_COLUMNS = 30;
	
	public static void main(String[] args){
		new TextArea();
	}
	public TextArea(){
		
		bank = new BankAccount("Pond");
		bank.deposit(1000);
		bank.withdraw(100);
		
		resultArea = new JTextArea(AREA_ROWS, AREA_COLUMNS);
		resultArea.setEditable(false);

		createTextField();
		createButton();
		createPanel();

		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	private void createPanel() {

		
		panel = new JPanel();
		panel.add(amountField);
		panel.add(withdrawButton);
		panel.add(depositButton);
		JScrollPane scrollPane = new JScrollPane(resultArea);
		panel.add(scrollPane); 
		add(panel);


		// TODO Auto-generated method stub
		
	}
	private void createButton() {
		withdrawButton = new JButton("withdraw");
		withdrawButton.addActionListener(listener);
		class withdrawListener implements ActionListener{
			public void actionPerformed(ActionEvent event){
				double amount = Double.parseDouble(amountField.getText());
				bank.withdraw(amount);
				resultArea.append(bank.getBalance() + "\n");
			}
		}

		depositButton = new JButton("deposit");
		depositButton.addActionListener(listener);
		class depositListener implements ActionListener{
			public void actionPerformed(ActionEvent event){
				double amount = Double.parseDouble(amountField.getText());
				bank.deposit(amount);
				resultArea.append(bank.getBalance() + "\n");
			}
		}

		// TODO Auto-generated method stub
		
	}
	private void createTextField() {
		final int FIELD_WIDTH = 10;
		amountField = new JTextField(FIELD_WIDTH);
		amountField.setText("   ");

	

	}

}
