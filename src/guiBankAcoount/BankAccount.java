package guiBankAcoount;

public class BankAccount {
	private String name;
	private double balance;
	public BankAccount(String name){
		this.name = name;
	}
	public void withdraw(double balance){
		this.balance-=balance;
		System.out.println(balance);
	}
	public void deposit(double balance){
		this.balance+=balance;
		System.out.println(balance);
	}
	public double getBalance(){
		return balance;
	}
	public String toString(){
		return "Balance of "+name+" : "+getBalance()+" bath.";
	}
	public String getName(){
		return name;
	}

}
