package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Combo_Box extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel, colrolPanel;
	private JComboBox colorBox;
	private ActionListener listener;
	public static void main(String[] args){
		new Combo_Box();
	}
	
	public Combo_Box(){
		listener = new ChoiceListener();
		createPanel();
		setColorPanel();
		
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}


	private void createPanel() {
		// TODO Auto-generated method stub
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		
		colorBox = new JComboBox();
		colorBox.addItem("RED");
		colorBox.addItem("GREEN");
		colorBox.addItem("BLUE");
		colorBox.setEditable(true);
		colorBox.addActionListener(listener);
		
		add(colorBox, BorderLayout.SOUTH);
	}
	
	class ChoiceListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			setColorPanel();
		}
	}
	private void setColorPanel() {
		
		int R = 0;
		int G =0;
		int B=0;
		
		if("RED"== colorBox.getSelectedItem()){
			R=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		else if ("GREEN"== colorBox.getSelectedItem()){
			G=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		
		else if("BLUE"== colorBox.getSelectedItem()){
			B=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		
	}
	
	

}
