package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.Radio_Button.ChoiceListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Menu extends JFrame{
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private ActionListener listener;
	private JPanel colorPanel;
	
	public static void main(String[] args){
		new Menu();
	}
	public Menu(){
		createPanel();
		
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void createPanel() {
		// TODO Auto-generated method stub
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		
		JMenu colorMenu = new JMenu("Color");
		
		colorMenu.add(createColorItem("RED",255,0,0));
		colorMenu.add(createColorItem("GREEN",0,255,0));
		colorMenu.add(createColorItem("BLUE",0,0,255));
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(colorMenu);
		
	}
	
	public JMenuItem createColorItem(String name,int R,int G,int B){
		JMenuItem item = new JMenuItem(name);
		class ManuItemListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				colorPanel.setBackground(new Color(R,G,B));
			}
		}
		ActionListener listener = new ManuItemListener();
		item.addActionListener(listener);
		return item;
		
	}
}
