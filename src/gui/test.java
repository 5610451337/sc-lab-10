package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class test extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colerPanel,radioB;
	private JRadioButton red,green,blue;
	
	public static void main(String[] args){
		new test();
	}
	public test(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(400,150);
	}
	public void createPanel(){
		colerPanel = new JPanel();
		colerPanel.setBackground(new Color(255,46,46));
		add(colerPanel,BorderLayout.CENTER);
		
		radioB = new JPanel();
		red = new JRadioButton("Red");
		green = new JRadioButton("Green");
		blue = new JRadioButton("Blue");
		red.setSelected(true);
		
		ButtonGroup group = new ButtonGroup();
		group.add(red);
		group.add(green);
		group.add(blue);
		
		radioB.add(red);
		radioB.add(green);
		radioB.add(blue);
		add(radioB,BorderLayout.SOUTH);
		
		red.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				colerPanel.setBackground(new Color(255,46,46));
			}
		});
		
		green.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				colerPanel.setBackground(new Color(25,255,52));
			}
		});
		blue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				colerPanel.setBackground(new Color(12,29,255));
			}
		});
	}
}
