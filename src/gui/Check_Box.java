package gui;

import gui.Radio_Button.ChoiceListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Check_Box extends JFrame{
	private static final long serialVersionUID = 1L;
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel,checkboxsPanel;
	private JCheckBox red ,green,blue;
	private ActionListener listener;
	public static void main(String[] args){
		new Check_Box();
	}
	public Check_Box(){
		listener = new ChoiceListener();
		createPanel();
		setColorPanel();
		
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private void createPanel() {
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER );
		
		checkboxsPanel = new JPanel();
		
		red = new JCheckBox("RED");
		red.addActionListener(listener);
		
		green = new JCheckBox("GREEN");
		green.addActionListener(listener);
		
		blue = new JCheckBox("BLUE");
		blue.addActionListener(listener);
		
		checkboxsPanel.add(red);
		checkboxsPanel.add(green);
		checkboxsPanel.add(blue);
		add(checkboxsPanel,BorderLayout.SOUTH);
		
		
		// TODO Auto-generated method stub
		
	}
	class ChoiceListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			setColorPanel();
		}
	}
	private void setColorPanel() {
		
		int R = 0;
		int G =0;
		int B=0;
		
		if(red.isSelected()){
			R=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		if (green.isSelected()){
			G=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		
		if(blue.isSelected()){
			B=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		
	}
}
