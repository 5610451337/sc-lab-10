package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Radio_Button extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel,radioButtonPanel;
	private JRadioButton red ,green,blue;
	private ActionListener listener;
	public static void main(String[] args){
		new Radio_Button();
	}
	public Radio_Button(){
		listener = new ChoiceListener();
		createPanel();
		setColorPanel();
		
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	private void setColorPanel() {
		
		int R = 0;
		int G =0;
		int B=0;
		
		if(red.isSelected()){
			R=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		else if (green.isSelected()){
			G=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		
		else if(blue.isSelected()){
			B=+255;
			colorPanel.setBackground(new Color(R,G,B));
		}
		
	}
	class ChoiceListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			setColorPanel();
		}
	}
	private void createPanel() {
		// TODO Auto-generated method stub
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		
		
		radioButtonPanel = new JPanel();
		red = new JRadioButton("RED");
		red.addActionListener(listener);
		
		green = new JRadioButton("GREEN");
		green.addActionListener(listener);
		
		blue = new JRadioButton("BLUE");
		blue.addActionListener(listener);
		
		ButtonGroup group = new ButtonGroup();
		group.add(red);
		group.add(green);
		group.add(blue);
		
		radioButtonPanel.add(red);
		radioButtonPanel.add(green);
		radioButtonPanel.add(blue);
		
		add(radioButtonPanel, BorderLayout.SOUTH);
	}
	
	
	
}
