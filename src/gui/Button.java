package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Button extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colorPanel,button;
	private JButton red ,green,blue;
	private ActionListener listener;
	public static void main(String[] args){
		new Button();

	}
	public Button(){
//		listener = new ChoiceListener();
		createPanel();
//		setColorPanel();
		
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	public void createPanel(){
		colorPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		
		button = new JPanel();
		red = new JButton("Red");
		red.addActionListener(listener);
		button.add(red);
		
		green = new JButton("Green");
		green.addActionListener(listener);
		button.add(green);
		
		blue = new JButton("Blue");
		blue.addActionListener(listener);
		button.add(blue);

		add(button, BorderLayout.SOUTH);
		
		class RedListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(255,0,0));
			}
		}
		class GreenListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,255,0));
			}
		}
		class BlueListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colorPanel.setBackground(new Color(0,0,255));
			}
		}
		
		red.addActionListener(new RedListener());
		green.addActionListener(new GreenListener());
		blue.addActionListener(new BlueListener());

		
		
	}
//	private void setColorPanel() {
//		
//		if(red.isSelected()){
//			colorPanel.setBackground(new Color(255,46,46));
//		}
//		else if (green.isSelected()){
//			colorPanel.setBackground(new Color(25,255,52));
//		}
//		
//		else if(blue.isSelected()){
//			colorPanel.setBackground(new Color(12,29,255));
//		}
//		
//	}
//	class ChoiceListener implements ActionListener{
//		public void actionPerformed(ActionEvent event){
//			setColorPanel();
//		}
//	}
	

}
